# Rondes

De verschillende robotrondes die de teams tijdens de finale spelen dienen ingesteld te worden via `Rondes` in het menu.

## Rondes toevoegen

Om een ronde toe te voegen klik op `toevoegen` rechts boven de tabel.
Dit opent een pop-up waar de ronde naam ingevuld kan worden. Vergeet niet om `opslaan` te drukken. De nieuwe ronde verschijnt nu in de tabel. De rondes worden in het formulier ook in deze volgorde getoond.

## Rondes verwijderen
Om de ronde te verwijderen klik je op het prullenbakje. Het prullenbakje verdwijnt automatisch zodra er wedstrijden zijn gereden in de finale

## Rondes publiceren

In elke rij staat ook een knopje met een oogje. Een groen open oogje betekend dat de ronde op dat moment openbaar staat. Alle wedstrijden in deze ronde zijn zichtbaar voor het publiek. 

Een rood oogje betekend dat een wedstrijd prive is. En niet zichtbaar is voor het publiek.

Om de status van een ronde te veranderen klik je op het oogje. Deze veranderd dan van status. Er is momenteel een bug waardoor je slechts 1x kan drukken per knop. Daarna moet de pagina ververst worden om nog een keer van status te veranderen.

## Advies

In veel regiofinales wordt er voor gekozen om de laatste robotronde niet te publiceren, zodat teams niet kunnen zien wie de Robotprestatie Award wint