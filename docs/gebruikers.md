# Gebruikers
Bij `gebruikers` kan je de verschillende gebruikers zien en verandereen.

## Gebruiker wijzigen
Om een gebruiker te wijzigen klik je op het blauwe knopje met het oogje. Nu opent zich het gebruikersmenu.
Alle velden kunnen worden gewijzigd. Er zijn momenteel veel rollen, niet alle rollen kunnen iets doen op het moment.
Om de wijzigen op te slaan klik op `opslaan`

## Wachtwoord wijzigen
In hetzelfde menu is een knop `wachtwoord wijzigen`. Je kunt op dit scherm een nieuw wachtwoord invullen. Dit word met `opslaan` meteen overscherven. Breng de gebruiker dan ook op de hoogte van zijn of haar nieuwe wachtwoord.

## Nieuwe gebruiker maken
Klik boven in de balk op `Toevoegen`

Hier staan verschillende velden, voor het email adres kan je een @fllscoring.nl-adres gebruiken. Er zit geen verificatie op, dus komt dat allemaal goed.

Klik in de `Role` lijst de rollen aan die deze gebruiker moet krijgen (hou CTRL in voor meerdere rollen) aan. Er zitten veel verschillende rollen in op dit niet in gebruik zijn. 

De gebruiker kan meteen inloggen zodra op submit is gedrukt

## Zelf registreren

Je kan ook mensen zelf een account laten maken via je link en /register er achter te zetten. Standaard hebben ze een `guest` account waarmee ze niets kunnen doen. Via het gebruikers menu kan je de rol van deze gebruikers veranderen om ze rechten te geven.