# Inloggen Scheidsrechter
Om in te loggen als scheidsrechter ga je naar de link / URL voor jou specifieke finale. Je begint je nu op de homepagina. Deze is leeg. Rechts bovenin staat het menu. Klik daar op inloggen

![Login scherm](/img/login_screen.png)

De scheidsrechter login is standaard
```
gebruikersnaam: ref@fllscoring.nl
wachtwoord: wachtwoord
```

Echter is het aan te raden om een scheidsrechters account per tafel aan te maken. Je kan hierna gebruik maken van de tafels functionaliteit. Het account van de scheidsrechter word dan gekoppeld aan de tafel. Het is dan gemakkelijker om terug te zoeken naar de scheidsrechter mochten er fouten zijn gemaakt in een wedstrijd.

Het aanmaken van gebruikers kunt u terug vinden in [Wedstrijdoverzicht](/wedstrijdoverzicht)