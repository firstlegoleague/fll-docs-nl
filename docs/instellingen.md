# Instellingen

Dit is het menu om de verschillende instellingen rondom je regiofinale aan te passen

![Instellingen](/img/settings.png)

## Naam finale
Dit is de naam van je finale. Deze staat op verschillende locaties vermeld. Mocht je deze niet willen moet je de waren zetten naar `NotSet`.

## Seizoen
Met deze optie kan je het formulier van seizoen veranderen wat ingevuld word door de scheidsrechter. Dit kan handig zijn voor offseasons of andere speciale evenementen. Mocht je een event organiseren met een seizoen of speciale regels die niet in de lijst staat, neem contact met ons op en we kunnen dan zien wat er mogelijk is.

## Gebruikers mogen registreren
Je kan scheidsrechters toestaan om zelf een account aan te maken. Dit word dan opgenomen in het formulier welke scheidsrechter het formulier heeft ingevuld. De gebruikers krijgen automatisch de "Gast" rol, je moet dit handmatig aanpassen

## Uitslag openbaar toegangelijk
Als dit vinkje aan staat verschijnt in het menu 2 extra tabjes. Dit zijn de openbare versies van [Wedstrijdoverzicht](/wedstrijdoverzicht) en [Wedstrijduitlagen](/wedstrijduitslagen).
Deze openbare versie is de vinden voor het publiek boven de login optie die standaard word gegeven.
