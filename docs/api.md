# API

This document will list the various API endpoints

All the API's will follow this scheme:

```
{region}.fllscoring.nl/api/{apikey}/{endpoint}
```

Where with the GET you can get the information provided by the endpoint and POST / DELETE / CHANGE you can do those various actions.

The actions that are supported by each endpoint are documented below

All additional information is formatted in json.

Responses will always come with a code and response. In case the code is an error, the response will have some information about what is wrong with the request

# keys
## GET
The GET request will return a list with all the API keys belonging to your account (Userid 1 (admin) will get all keys of all users)

## POST
This does not require any additional information. A new API key will be generated with the same user that requested this key
## DELETE
- id : the ID of the key that should be disabled.

# users
## GET
This will get all the users in the system.

## POST
This will create a new user

Required:

- name : The name of the new user
- email : The emailadress of the new user
- password : Password for the new user

Additional

- role : The role for the new user. (Can't create certain roles.)

## DELETE
This will remove a user

Required:

- id : The id of the user that shall be removed (Can not be yourself or super-admin)

## PATCH
Change something about this user. Can't change super-admin's

Required:

- id : The id of the user

Aditional

- name : New name for the user
- email : New email for the user
- password : New password for the user

# games
Interact with the games

## GET
Gets a list with all games from this final

additional:

 - id : To get a single game

## POST
Submit a game to this final

Required:

- Depends on the season. Check the documentation for that season

## DELETE
Removes a game from the final

Required:

- id : The ID of the game

## PATCH
Changes the outcome of a game

Required:

- Depends on the season, check the documentation for that season

# teams
Interact with the teams from this final
## GET
Get all the teams participating in this final

## POST
Create a new team

Required:

- teamnumber : The teamnumber for the new team
- teamname : The teamname for the new team

Additional:

- affiliate : The school / organization this team belongs to

## DELETE
Remove a team from the final

Required:

- id : The id of the team you want to remove

## PATCH
Change things for this team

Required:

- id : The ID of the team to change

Additional:

- teamnumber : The new teamnumber for the team
- teamname : The new teamname for the team
- affiliate : The new affiliate for the team

# settings
To get and patch settings. You can't delete or post settings.

## GET
To get all the current settings from a site

additional:

- key: To get a certain key from the settings

## PATCH
To set a certain key

Required:

- key: Which key to modify
- value: What the value of this key will be.