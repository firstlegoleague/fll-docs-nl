# Wedstrijdoverzicht

![Wedstrijd overzicht](/img/game_table.png)
Het wedstrijdoverzicht geeft een totaaloverzicht in de vorm van een tabel. Zo kan je meteen zien of er nog ergens scores missen of scores dubbel zijn ingevuld.

Een rood vakje in de tabel (aangeduid met -1) betekend dat dit team in deze ronde meerdere entries heeft.

Een wit vakje met 0 geeft aan dat er op dit moment nog geen score is. (of een score van 0. maar dit is erg onwaarschijnlijk)