# Scoreboard
Het scorebord heeft inzicht over de huidige ranking.

## Publiekelijke scorebord

Via het menu kom je terecht bij het scorebord wat de teams ook kunnen zien. De hoogste score staat boven aan. Bij een gelijke hoogste score word er gekeken naar de andere scores in de finale. Mochten deze ook gelijk het systeem een random team kiezen.

## Beamer scorebord

Door achter de publiekelijke link `/screen` komt er een speciale pagina bedoeld voor op grote schermen zoals beamers. De kleur van de achtergrond word aanpasbaar zodat deze met greenscreens er uit gefilterd kan worden.

## Prive scoreboard
Door achter de publiekelijke link `/private` te zetten, verschijnt er ook een scorebord. Deze is alleen toegangelijk voor ingelogde gebruikers en omzeilt de check of de rondes op publiek staan. Zo kan je snel een overzicht krijgen met filter.