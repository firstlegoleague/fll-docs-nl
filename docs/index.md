# FLL Scoring

Het scoring systeem is een webapplicatie, die je via de browser op je laptop, tablet of telefoon kunt openen. Via JetNet & TechNet ontvang je de link/URL naar het scoring systeem en de inloggegevens voor jouw regio. Mocht je een offseason of ander niet officieel FLL event organiseren en deze tool gebruiken, kan je contact opnemen met [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl). Iedere regio/finale heeft een andere link/URL. Ben je beheerder van verschillende regiofinales, dan krijg je voor iedere finale een andere link en inlog gegevens.

De FLL scoring tool is gemaakt voor JetNet & TechNet door Thijs Tops. Al het werk in deze tool is vrijwillig. Mocht je mee willen werken aan het onderhouden en bijhouden van de tool mag dit! De broncode is te vinden op [GitLab](https://gitlab.com/firstlegoleague/fll-score-system)

Bug en andere issues kunnen ook op de GitLab worden achter gelaten of worden opgestuurd naar [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl)

## Contributers
Thijs Tops


## Opmerkingen
De documentatie is geschreven voor de scoringtool van 2022, de versie die jullie gaan gebruiken is van 2023, alle functies zijn nog beschikbaar, maar niet alle nieuwe functies zullen beschreven zijn. Of zijn nu onder een ander kopje te vinden, bij vragen mogen jullie altijd contact opnemen met mij via [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl)