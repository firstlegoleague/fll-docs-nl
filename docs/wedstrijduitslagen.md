# Wedstrijduitslagen

Het wedstrijdoverzicht geeft een totaaloverzicht van alle ingevoerde scores per team/ronde.
Het overzicht kan worden gedownload naar Excel, zodat de scores naar het Officiële juryspreadsheet gekopieerd kunnen worden. 

## Filters
Standaard staan de wedstrijden gesorteerd op ID (volgorde van insturen door de scheidsrechter). Dit kan worden aangepast door op de colommen te drukken.

## Downloaden Scores

Het overzicht kan gedownload worden naar excel, in het format zoals gevraagd door het Officiële juryspreadsheet. Om dit te doen klik boven aan op de knop `Download Scores (OJS)`. Dit zal een excel genereren en automatisch opslaan met alle gegevens hierin. De eerste colommen zijn de scores. De opvolgende colommen zijn de Gracious professialism scores gegeven door de scheidsrechters op de wedstrijd.

## Downloaden Opmerkingen

De scheidsrechters kunnen ook opmerkingen achter laten bij de wedstrijd. Deze worden ook in het overzicht laten zien, maar voor het gemak zijn deze ook downloadbaar. Klik hiervoor op de knop `Download Opmerkingen`
