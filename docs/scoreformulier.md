# Nieuw formulier

Dit opent een nieuw formulier, welk formulier geopend word kan worden ingesteld via [Instellingen](/instellingen). Bij oplevering van het systeem zal deze automatisch staan ingesteld dat dit het huidige seizoen is. 

Dit menu word verder uitgelegd in [Invullen Scoreformulier](/invullen_scoreformulier). 
Voor de scheidsrechters is er een speciaal account standaard mee geleverd, maar de mogelijkheid is er om voor elke scheidsrechter een apart account aan te maken. Hier door kan eventueel later worden ingezien welke scheidsrechter welk formulier heeft ingevuld.
