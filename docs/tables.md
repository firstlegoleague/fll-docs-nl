# Tafels

Het is mogelijk om een tafel mee te geven bij de wedstrijd zodat het gemakkelijker is om dit bij te houden. Deze dien toegevoegd te worden.

## Tafels toevoegen

Om een tafel toe te voegen klik op `toevoegen` rechts boven de tabel.
Dit opent een pop-up waar de tafel naam ingevuld kan worden. Vergeet niet om `opslaan` te drukken. De nieuwe tafel verschijnt nu in de tabel. De tafels worden in het formulier ook in deze volgorde getoond.

## Tafel verwijderen
Om de tafel te verwijderen klik je op het prullenbakje. Het prullenbakje verdwijnt automatisch zodra er wedstrijden zijn gereden op deze tafel

## Advies / Opmerking
De tafel word automatisch ingevuld op het formulier op basis van de laatst gekozen tafel, zodat scheidsrechters dit niet constant opnieuw hoeven te doen. Hierdoor is het aanbevolen om meerdere scheidsrechters (minimaal 1 per tafel) aan te maken, of te kiezen voor slechts 1 tafel voor alle wedstrijden in de software