# Timer

De timer is de klok die tijdens de wedstrijden gebruikt kan worden om de tijdsduur van 2,5 minuut op een scherm weer te geven. De klok telt af naar 0 en geeft de bekende geluiden weer bij de start van de robotwedstrijd en het einde van de robotwedstrijd.

De functionaliteit om te timers synchroon aan te zetten ingebouwd, dit kan nu bijv met een knop worden gedaan. Het starten, stoppen en resetten van de timers kan vanuit het instellingen menu

#Issues
## Edge

Edge vind het niet leuk als media automatisch afspeelt, daarom moet je via deze pagina uw website toevoegen aan de toegestaande lijst
edge://settings/content/mediaAutoplay


## Chrome / Firefox
Eerst keer handmatig op start drukken op elk scherm, dit om autoplay aan te zetten. Hierna zal de sync functie werken.

## Mixed browsers

Gebruik op alle locaties dezelfde browser (Firefox, chrome, edge). Het gebruik van verschillende browsers kan leiden tot verschillende tijden.
