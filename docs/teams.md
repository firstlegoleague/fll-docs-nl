# Teams

Met behulp van de menukeuze ‘Teams’ kun je de gegevens van de teams die meedoen aan de finale toevoegen, wijzigen of verwijderen.

Op dit moment kunnen teams alleen handmatig, team per team, worden toegevoegd. JetNet zal voor grote finales de teamgegevens vanuit het registratiesysteem importeren, zodat dit niet handmatig hoeft te gebeuren.

## Team handmatig toevoegen

Rechts boven in de tabel staat een knop `Toevoegen`. 

![Team toevoegen](/img/team_create.png)

Geef het teamnummer, de teamnaam en eventueel de bijbehorende school / organisatie / vrienden groep in. Klik hierna op `Opslaan`

Het team zal nu in de lijst verschijnen.

## Teamgegevens wijzigen of verwijderen

In elke rij staat een blauwe knop met een oogje. Dit opent een nieuwe pagina om het team aan te passen of te verwijderen.

Wijzig de gegevens en klik op `opslaan`.

**Let op:** Het teamnummer kan niet aangepast worden. Indien er een onjuist teamnummer is ingevuld, dienen de gegevens te worden verwijderd en vervolgens met het juiste teamnummer toegevoegd te worden.

Om te verwijderen klik onderaan de op de knop `verwijderen`

**Let op:** Indien een team al resultaten heeft behaald tijdens de wedstrijd is het niet meer mogelijk om dit team te verwijderen.

