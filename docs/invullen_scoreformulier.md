# Invullen Scoreformulier

Als scheidsrechter klik op de knop `nieuw formulier` in het menu.
Het scoreformulier word nu geopend.

## Team en ronde selecteren
Het eerste blokje gaat over de team en de ronde. Voor het team is dit een autofill veld. Begin met typen, en de site zal beginnen met aanvullen en suggesties geven. Selecteer er een uit deze lijst, anders kan er later iets fout gaan.

De ronde is een dropdown menu. Selecteer hier de huidige ronde in. Mocht je een ronde perongeluk dubbel invullen, laat je wedstrijdsecretariaat dit dan weten. Deze kan dit oplossen. Er worden geen gegevens overschreven!

## Wedstrijd beoordelen
Aan het einde kan je het formulier invullen. Doe dit secuur zodat je geen team benadeeld. De site heeft bij enkele missies hulpjes, waardoor het onmogelijke situaties zal proberen te voorkomen en zal je dan ook waarschuwen.

![Missie fout](/img/ref_helper.png)

## Score berekenen

Als alles is ingevuld kan je een opmerking geven over het team voor het jury beraad of als opmerking voor het wedstrijdsecretariaat. 
Klik hierna op de knop `Controleer scoreformulier en bereken score`. Deze zal alles nogmaals nakijken en berekenen. Mocht er iets niet goed ingevuld zijn zal dit onderdeel rood worden.

![Missie fout](/img/mission_wrong.png)

Corrigeer deze fout en controleer op nieuw.

Als alles goed is zal de knop `verstuur` actief worden. **Vergeet deze niet in te drukken, anders zal de score niet verstuurd worden**

![Versturen](/img/scoreform_correct.png)
