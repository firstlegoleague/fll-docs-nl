# Inloggen Beheerder

Om in te loggen als beheerder ga je naar de link / URL voor jou specifieke finale. Je begint je nu op de homepagina. Deze is leeg. Rechts bovenin staat het menu. Klik daar op inloggen


![Login scherm](/img/login_screen.png)


Hier vul je de gegevens in die je hebt gekregen. Als alles goed gaat ben je nu ingelogd en staan er nu meer opties in het menu. Deze worden verder uitgelegd op deze site.