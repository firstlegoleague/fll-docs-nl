# Account

Om je wachtwoord te wijzigen kies je voor ‘Account’

 - Klik op ‘Account’
 - Kies [Wijzig je wachtwoord]
 - Geef je huidige wachtwoord in en 2x het nieuwe wachtwoord
 - Klik op [Veranderen]